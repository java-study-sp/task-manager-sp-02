package com.morozov.tm.api.service;

import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.model.dto.TaskDto;
import com.morozov.tm.model.entity.Project;
import com.morozov.tm.model.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    @NotNull
    TaskDto findById(String id) throws TaskNotFoundException;

    @NotNull
    List<TaskDto> findAllTask();

    void addTask(@NotNull String taskName, @NotNull String projectId) throws ProjectNotFoundException;

    void deleteTaskById(@NotNull String id);

    void updateTask(@NotNull String id, @NotNull String name,
                    @NotNull String description, @NotNull String dataStart, @NotNull String dataEnd,
                    @NotNull String projectId) throws ParseException, TaskNotFoundException, ProjectNotFoundException;

    @NotNull
    List<TaskDto> findAllTaskByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDto> searchByString(@NotNull String string);

    void deleteAllTask();

    @NotNull
    TaskDto transferTaskToTaskDto(@NotNull Task task);

    @NotNull
    Task transferTaskDtoToTask(@NotNull TaskDto taskDto, @NotNull final Project project);

    @NotNull
    List<TaskDto> transferListTaskToListTaskDto(@NotNull List<Task> taskList);

}
