package com.morozov.tm.model.dto;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class AbstractEntityDto implements Serializable {
    @NotNull
        private String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
