package com.morozov.tm.model.dto;

import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.model.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class AbstractWorkEntityDto extends AbstractEntity {
    @NotNull
    private String name = "";
    @NotNull
    private Date createdData = new Date();
    @NotNull
    private String userId = "";
    @NotNull
    private String description = "";
    @Nullable
    private Date startDate = null;
    @Nullable
    private Date endDate = null;
    @NotNull
    private StatusEnum status = StatusEnum.PLANNED;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedData() {
        return createdData;
    }

    public void setCreatedData(Date createdData) {
        this.createdData = createdData;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
