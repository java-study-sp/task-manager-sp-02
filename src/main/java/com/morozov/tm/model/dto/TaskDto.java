package com.morozov.tm.model.dto;

import org.jetbrains.annotations.NotNull;

public class TaskDto extends AbstractWorkEntityDto {
    @NotNull
    private String idProject = "";

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }
}
