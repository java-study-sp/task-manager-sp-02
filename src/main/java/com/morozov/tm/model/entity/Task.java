package com.morozov.tm.model.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app_task")
public class Task extends AbstractWorkEntity {

    @Nullable
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Project project;

    @Nullable
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Task() {
    }
}
