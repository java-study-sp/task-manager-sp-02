package com.morozov.tm.service;

import com.morozov.tm.api.repository.IProjectRepository;
import com.morozov.tm.api.repository.ITaskRepository;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.model.dto.TaskDto;
import com.morozov.tm.model.entity.Project;
import com.morozov.tm.model.entity.Task;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TaskService implements ITaskService {
    @Autowired
    private ITaskRepository taskRepository;
    @Autowired
    private IProjectRepository projectRepository;


    @NotNull
    @Override
    public TaskDto findById(String id) throws TaskNotFoundException {
        @NotNull Task task = taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
        @NotNull TaskDto taskDto = transferTaskToTaskDto(task);
        return taskDto;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllTask() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        return transferListTaskToListTaskDto(taskList);
    }

    @Override
    public void addTask(@NotNull final String taskName, @NotNull final String projectId) throws ProjectNotFoundException {
        @NotNull final Task task = new Task();
        task.setName(taskName);
        @NotNull final Project project = projectRepository.findById(projectId).orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        taskRepository.save(task);
    }


    @Override
    public void updateTask(
            @NotNull final String id, @NotNull final String name, @NotNull final String description,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final String projectId)
            throws ParseException, TaskNotFoundException, ProjectNotFoundException {
        @NotNull Task task = taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        final Date updatedStartDate = DateFormatUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            task.setStatus(StatusEnum.PROGRESS);
        }
        task.setStartDate(updatedStartDate);
        final Date updatedEndDate = DateFormatUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            task.setStatus(StatusEnum.READY);
        }
        task.setEndDate(updatedEndDate);
        @NotNull final Project project = projectRepository.findById(projectId).orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        taskRepository.save(task);
    }


    @Override
    public @NotNull List<TaskDto> findAllTaskByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> resultTaskList = taskRepository.findAllTaskByProjectId(projectId);
        return transferListTaskToListTaskDto(resultTaskList);
    }

    @Override
    public @NotNull List<TaskDto> searchByString(@NotNull String string) {
        if (string.isEmpty()) {
            return new ArrayList<>();
        }
        @NotNull List<Task> taskList = taskRepository.searchByString(string);
        return transferListTaskToListTaskDto(taskList);
    }

    @Override
    public void deleteTaskById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    public void deleteAllTask() {
        taskRepository.deleteAll();
    }

    @Override
    public @NotNull TaskDto transferTaskToTaskDto(@NotNull Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setCreatedData(task.getCreatedData());
        taskDto.setStartDate(task.getStartDate());
        taskDto.setEndDate(task.getEndDate());
        taskDto.setStatus(task.getStatus());
        //if (task.getUser() != null) taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null) taskDto.setIdProject(task.getProject().getId());
        return taskDto;
    }

    @Override
    @NotNull
    public Task transferTaskDtoToTask(@NotNull TaskDto taskDto, @NotNull final Project project) {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setCreatedData(taskDto.getCreatedData());
        task.setStartDate(taskDto.getStartDate());
        task.setEndDate(taskDto.getEndDate());
        task.setStatus(taskDto.getStatus());
        //task.setUser(user);
        task.setProject(project);
        return task;
    }

    @Override
    public @NotNull List<TaskDto> transferListTaskToListTaskDto(@NotNull List<Task> taskList) {
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            taskDtoList.add(transferTaskToTaskDto(task));
        }
        return taskDtoList;
    }
}
