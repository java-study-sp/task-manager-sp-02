<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Task List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
<div class="container-fluid">
    <div>
        <h1>Task List</h1>
    </div>
    <div>
        <c:choose>
            <c:when test="${!tasklist.isEmpty()}">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Project ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Data Create</th>
                        <th scope="col">Data Begin</th>
                        <th scope="col">Data End</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${tasklist}" var="task">
                        <tr>
                            <td scope="row">${task.id}</td>
                            <td scope="row">${task.idProject}</td>
                            <td scope="row">${task.name}</td>
                            <td scope="row">${task.description}</td>
                            <td scope="row">${task.createdData}</td>
                            <td scope="row">${task.startDate}</td>
                            <td scope="row">${task.endDate}</td>
                            <td scope="row">${task.status}</td>
                            <td scope="row"><a href="/tasks/view/${task.id}/">VIEW</a></td>
                            <td scope="row"><a href="/tasks/edit/${task.id}/">EDIT</a></td>
                            <td scope="row"><a href="/tasks/delete/${task.id}/">REMOVE</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <form action="/tasks/deleteAll" method="get">
                    <button type="submit" class="btn btn-primary">Delete all tasks</button>
                </form>
            </c:when>
            <c:otherwise>
                <h3>Task list is empty</h3>
            </c:otherwise>
        </c:choose>
        <form action="/tasks/add" method="get">
            <button type="submit" class="btn btn-primary">Create Task</button>
        </form>
    </div>
</div>
</body>
</html>
