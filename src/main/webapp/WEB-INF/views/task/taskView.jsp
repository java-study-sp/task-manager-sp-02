<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Task View</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
<div class="container-fluid">
    <div>
        <h1>Task View</h1>
    </div>
    <div class="row">
        <div class="col-5">
            <div class="list-group">
                <div class="list-group-item list-group-item-action active">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">${task.name}</h5>
                    </div>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">ID:</h5>
                    </div>
                    <p class="mb-1">${task.id}</p>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Project ID:</h5>
                    </div>
                    <p class="mb-1">${task.idProject}</p>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Description:</h5>
                    </div>
                    <p class="mb-1">${task.description}</p>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Date create:</h5>
                    </div>
                    <p class="mb-1">${task.createdData}</p>
                </div>
                <c:if test="${task.startDate != null}">
                    <div class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Date begin:</h5>
                        </div>
                        <p class="mb-1">${task.startDate}</p>
                    </div>
                </c:if>
                <c:if test="${task.endDate != null}">
                    <div class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Date end:</h5>
                        </div>
                        <p class="mb-1">${task.endDate}</p>
                    </div>
                </c:if>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Status:</h5>
                    </div>
                    <p class="mb-1">${task.status}</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
